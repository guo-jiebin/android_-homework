package com.example.homework7;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class preferencesFragment extends Fragment {
    private Button button;
    private TextView textView1,textView2;
    public preferencesFragment() {
        super(R.layout.sharedpreferences);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button = getView().findViewById(R.id.button);
        textView1 = getView().findViewById(R.id.username);
        textView2 = getView().findViewById(R.id.password);
        SharedPreferences settings = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);
        String username = settings.getString("username","");
        String password = settings.getString("password","");
        textView1.setText(username);
        textView2.setText("*********");

        textView2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                textView2.setText("");
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("username",textView1.getText().toString());
                if(textView2.getText().toString().length()==64){
                    editor.putString("password",password);
                }else{
                    editor.putString("password",Sha256.getSHA256(textView2.getText().toString()));
                }
                editor.commit();
                Toast.makeText(getActivity(),"保存成功",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
