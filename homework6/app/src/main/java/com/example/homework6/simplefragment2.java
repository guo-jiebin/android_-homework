package com.example.homework6;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class simplefragment2  extends Fragment {
    private Button button1,button2,button3;
    public simplefragment2(){
        super(R.layout.simple_fragment2);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button1 = getView().findViewById(R.id.button);
        button2 = getView().findViewById(R.id.button2);
        button3 = getView().findViewById(R.id.button3);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent();
                intent1.setData(Uri.parse("https://www.baidu.com"));
                startActivity(intent1);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = getActivity().getPackageManager().getLaunchIntentForPackage("tv.danmaku.bili");
                startActivity(intent2);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = getActivity().getPackageManager().getLaunchIntentForPackage("com.tencent.qqmusic");
                startActivity(intent3);
            }
        });
    }


}
