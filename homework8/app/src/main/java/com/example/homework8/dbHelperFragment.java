package com.example.homework8;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class dbHelperFragment extends Fragment {
    private Button button1,button2;
    private EditText editText1,editText2;
    private TextView editText3;
    DbHelper dbHelper;
    String username,age;
    public  dbHelperFragment(){
        super(R.layout.sqlite_activity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button1 = getView().findViewById(R.id.add);
        button2 = getView().findViewById(R.id.check);
        editText1 = getView().findViewById(R.id.name);
        editText2 = getView().findViewById(R.id.age);
        editText3 = getView().findViewById(R.id.contentTextView);

        dbHelper = new DbHelper(getActivity()) ;
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                username = editText1.getText().toString();
                age = editText2.getText().toString();
                values.put("username",username);
                values.put("age",age);

                long ret = db.insert("users",null,values);
                String msg = ret == -1?"添加失败":"添加成功";
                Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
                db.close();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                StringBuilder stringBuilder = new StringBuilder();
                String selection = "age<18";
                //参数1：表名 参数2：查询结果包含那些列，null代表所有列 参数3：查询条件 参数4：替换参数3中农的问号 参数5：分组方式 参数6：having 参数7：排序orderBy
                Cursor cursor = db.query("users",null,selection,null,null,null,null);
                while(cursor.moveToNext()){
                    long id = cursor
                            .getLong(cursor.getColumnIndex("id"));
                    String username = cursor.getString(cursor.getColumnIndex("username"));
                    int age = cursor.getInt(cursor.getColumnIndex("age"));
                    stringBuilder.append("id：").append(id)
                            .append("   username：").append(username)
                            .append("   age：").append(age)
                            .append("\n");

                }
                editText3.setText(stringBuilder.toString());

            }
        });


    }


}
