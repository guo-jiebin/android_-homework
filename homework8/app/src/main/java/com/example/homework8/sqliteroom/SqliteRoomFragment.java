package com.example.homework8.sqliteroom;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.homework8.R;

import java.util.List;

public class SqliteRoomFragment extends Fragment {
    private Button button1,button2;
    private EditText editText1,editText2;
    private TextView textView3;
    MyRoomDatabase db;
    public  SqliteRoomFragment(){
        super(R.layout.sqlite_activity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = MyRoomDatabase.getInstance(getActivity());
        button1 = getView().findViewById(R.id.add);
        button2 = getView().findViewById(R.id.check);
        editText1 = getView().findViewById(R.id.name);
        editText2 = getView().findViewById(R.id.age);
        textView3 = getView().findViewById(R.id.contentTextView);
        showUsers(db.userDao().getAll());
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = editText1.getText().toString();
                String age = editText2.getText().toString();
                if(username!=null&&age!=null){
                    User user = new User();
                    user.setUsername(username);
                    user.setAge(Integer.parseInt(age));
                    db.userDao().insert(user);
                }

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUsers(db.userDao().getUserByAge(18));
            }
        });

    }

    private void showUsers(List<User> userList){
        StringBuilder stringBuilder = new StringBuilder();
        for (User user :userList){
            stringBuilder.append("id：").append(user.id)
                    .append("   username：").append(user.username)
                    .append("   age：").append(user.age)
                    .append("\n");
        }
        textView3.setText(stringBuilder.toString());
    }
}
