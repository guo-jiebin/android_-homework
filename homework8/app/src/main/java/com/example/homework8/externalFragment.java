package com.example.homework8;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class externalFragment extends Fragment {
    private Button button3,button4;
    public externalFragment() {
        super(R.layout.storageexternal);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button3 = getView().findViewById(R.id.button3);
        button4 = getView().findViewById(R.id.button4);

        File externalPath = getActivity().getExternalFilesDir(null);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(externalPath,"external.txt");
                try {
                    FileOutputStream os = new FileOutputStream(file);
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write("test");
                    writer.close();
                    Toast.makeText(getActivity(),"写入成功",Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(externalPath,"external.txt");
                try {
                    FileInputStream is = new FileInputStream(file);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String strLine = reader.readLine();
                    Toast.makeText(getActivity(),strLine,Toast.LENGTH_SHORT).show();
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
