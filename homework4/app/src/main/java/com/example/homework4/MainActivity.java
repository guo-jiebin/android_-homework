package com.example.homework4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button;
        button = findViewById(R.id.button2);
        EditText editText;
        editText = findViewById(R.id.editTextPhone);
        EditText editText2;
        editText2 = findViewById(R.id.editTextNumber);
        EditText editText3;
        editText3 = findViewById(R.id.editTextTextPassword);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                intent.setAction("android.intent.action.View");
                intent.putExtra("com.example.homework4.editTextPhone",editText.getText().toString());
                intent.putExtra("com.example.homework4.editTextNumber",editText2.getText().toString());
                intent.putExtra("com.example.homework4.editTextTextPassword",editText3.getText().toString());
                startActivity(intent);
            }
        });
    }


}