package com.example.homework4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    TextView textView1,textView2,textView3;
    Button button,button2,button3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        String editTextPhone = intent.getStringExtra("com.example.homework4.editTextPhone");
        String editTextNumber = intent.getStringExtra("com.example.homework4.editTextNumber");
        String editTextTextPassword = intent.getStringExtra("com.example.homework4.editTextTextPassword");
        textView1 = findViewById(R.id.phone);
        textView1.setText(editTextPhone);
        textView2 = findViewById(R.id.yanzhengma);
        textView2.setText(editTextNumber);
        textView3 = findViewById(R.id.password);
        textView3.setText(editTextTextPassword);

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent();
                intent1.setData(Uri.parse("https://www.baidu.com"));
                startActivity(intent1);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = getPackageManager().getLaunchIntentForPackage("tv.danmaku.bili");
                startActivity(intent2);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = getPackageManager().getLaunchIntentForPackage("com.tencent.qqmusic");
                startActivity(intent3);
            }
        });
    }
}