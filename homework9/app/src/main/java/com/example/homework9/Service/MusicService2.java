package com.example.homework9.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MusicService2 extends Service {
    MusicHelper musicHelper = null;
    public MusicService2() {
    }
    @Override
    public void onCreate() {
        Log.d("MusicService---------","onCreate");
        super.onCreate();
        musicHelper = new MusicHelper(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MusicService---------","onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("MusicService---------","onDestroy");
        super.onDestroy();
        musicHelper.destroy();
        musicHelper = null;
    }

    public class MyBinder extends Binder{
        private MusicService2 service;
        public MyBinder(MusicService2 service){
            this.service = service;
        }

        public void play(){service.musicHelper.play();}
        public void next(){service.musicHelper.next();}
        public void pause(){service.musicHelper.pause();}
        public void destroy(){service.musicHelper.destroy();}

    }
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MusicService---------","onBind");
        return new MyBinder(this);
    }
}