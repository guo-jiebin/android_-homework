package com.example.homework9.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MusicService extends Service {
    private MusicHelper musicHelper = null;

    public MusicService() {
    }

    //重写Service的方法onCreate、onStartCommand、onDestroy
    @Override
    public void onCreate() {
        Log.d("MusicService---------","onCreate");
        super.onCreate();
        musicHelper = new MusicHelper(this);
    }

    //Service服务启动就执行
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        musicHelper.play();
        Log.d("MusicService---------","onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("MusicService---------","onDestroy");
        super.onDestroy();
        musicHelper.destroy();
        musicHelper = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}