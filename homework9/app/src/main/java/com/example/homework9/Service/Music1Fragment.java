package com.example.homework9.Service;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.homework9.R;

public class Music1Fragment extends Fragment {
    private Button button1,button2;
    public Music1Fragment() {
        super(R.layout.service1_activity);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button1 = getView().findViewById(R.id.start1);
        button2 = getView().findViewById(R.id.stop1);
        //播放
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),MusicService.class);
                getActivity().startService(intent);//启动服务
            }
        });

        //停止
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),MusicService.class);
                getActivity().stopService(intent);//暂停服务
            }
        });
    }
}
