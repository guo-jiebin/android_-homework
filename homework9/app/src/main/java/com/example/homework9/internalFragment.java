package com.example.homework9;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class internalFragment extends Fragment {
    private Button button1,button2;
    public internalFragment() {
        super(R.layout.storageinternal);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button1 = getView().findViewById(R.id.button1);
        button2 = getView().findViewById(R.id.button2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream os = getActivity().openFileOutput("internal.txt", Context.MODE_PRIVATE);
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write("test");
                    writer.close();
                    Toast.makeText(getActivity(),"写入成功",Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream is = getActivity().openFileInput("internal.txt");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String strLine =  reader.readLine();
                    Toast.makeText(getActivity(),strLine,Toast.LENGTH_SHORT).show();
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
