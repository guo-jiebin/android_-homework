package com.example.homework9.Service;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import com.example.homework9.R;

import java.io.IOException;


public class MusicHelper {
    private MediaPlayer mediaPlayer;
    private Context context;
    private final int[] musics = new int[]{R.raw.music1,R.raw.music2};
    private int musicIndex = 0;
    private boolean prepared = false;

    public MusicHelper(Context context){
        this.context =context;
        createMediaPlayer();
    }
    private void createMediaPlayer(){
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    //播放音频
    public void play(){
        //检测是否正在播放
        if(mediaPlayer.isPlaying()){
            return;
        }
        //检测配置是否已经配好，配好了就直接播放
        if (prepared){
            mediaPlayer.start();
            return;
        }
        //mediaPlayer进行配置
        try {
            mediaPlayer.setDataSource(context, Uri.parse("android.resource://com.example.homework9/"+musics[musicIndex]));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaPlayer.prepareAsync();//prepare过程中不允许操作按钮
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                prepared = true;
            }
        });
    }
    //暂停
    public void pause(){
        if(!mediaPlayer.isPlaying()){
            return;
        }
        mediaPlayer.pause();
    }
    //下一首
    public void next(){
        musicIndex = musicIndex+1;
        musicIndex = musicIndex % musics.length;
        destroy();//销毁mediaPlayer
        createMediaPlayer();//重新创建一个mediaPlayer
        play();//播放新的index对应的音乐
    }

    public void destroy(){
        if(mediaPlayer == null){
            return ;
        }
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
        prepared = false;
    }
}
