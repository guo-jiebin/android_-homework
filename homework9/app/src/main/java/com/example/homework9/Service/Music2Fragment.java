package com.example.homework9.Service;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.homework9.R;

public class Music2Fragment extends Fragment {
    private Button button1,button2,button3;
    private MyConn myConn = null;
    public Music2Fragment() {
        super(R.layout.service2_activity);
    }

    public class MyConn implements ServiceConnection{
        public MusicService2.MyBinder myBinder = null;
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            myBinder = (MusicService2.MyBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        button1 = getView().findViewById(R.id.start2);
        button2 = getView().findViewById(R.id.next2);
        button3 = getView().findViewById(R.id.stop2);

        Intent intent = new Intent(getActivity(),MusicService2.class);
        getActivity().startService(intent);
        if(myConn == null){
            myConn = new MyConn();
            intent = new Intent(getActivity(),MusicService2.class);
            getActivity().bindService(intent,myConn,0);
        }

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myConn.myBinder == null){
                    return;
                }

                myConn.myBinder.play();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myConn.myBinder == null){
                    return;
                }
                myConn.myBinder.next();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myConn.myBinder == null){
                    return;
                }
                myConn.myBinder.pause();
            }
        });

    }
}
