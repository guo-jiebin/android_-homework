package com.example.homework3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

public class CustomDialogFragment extends DialogFragment {
    private NotifyDialogListener listener;
    private EditText name_edit;
    private EditText sex_edit;
    public interface NotifyDialogListener{
        public void onDialogLoginClicked();
    }
    public void setListener(NotifyDialogListener listener){
        this.listener = listener;
    }

    public String getName_edit() {
        return name_edit.getText().toString();
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_dialog,null);
        name_edit = view.findViewById(R.id.name_text);
        sex_edit = view.findViewById(R.id.sex_text);
        builder.setView(view);
        builder.setTitle("自定义多选框");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onDialogLoginClicked();
            }
        });
        return builder.create();
    }
}
