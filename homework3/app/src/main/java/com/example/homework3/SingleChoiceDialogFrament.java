package com.example.homework3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;


import androidx.fragment.app.DialogFragment;

public class SingleChoiceDialogFrament extends DialogFragment {
    private String[] genders= new String[]{"男","女"};
    private String gender;
    private NotifyDialogListener listener;

    public interface  NotifyDialogListener{
        public void onDialogPositiveClick();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setListener(NotifyDialogListener listener){
        this.listener = listener;
    }


    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("单选对话框")
                .setSingleChoiceItems(genders, 0,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gender = genders[which];
                    }
                })
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogPositiveClick();
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity().getApplicationContext(),"取消了操作",Toast.LENGTH_SHORT).show();
                    }
                });
        return builder.create();
    }
}
