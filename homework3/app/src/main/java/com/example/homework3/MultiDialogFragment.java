package com.example.homework3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;


import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.Iterator;

public class MultiDialogFragment extends DialogFragment {
    private String[] genders = {"男","女"};
    private NotifyDialogListener listener;
    private ArrayList selectedItems;

    public interface NotifyDialogListener{
        public void onDialogSelectedClicked();
    }
    public void setListener(NotifyDialogListener listener){
        this.listener = listener;
    }
    public String getGenders(){
        String result = "";
        Iterator iterator = selectedItems.iterator();
        while(iterator.hasNext()){
            result += iterator.next()+"&";
        }
        return result.substring(0,result.length()-1);
    }


    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState) {
        selectedItems = new ArrayList();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("多选对话框")
                .setMultiChoiceItems(genders, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            selectedItems.add(genders[which]);
                        } else {
                            selectedItems.remove(genders[which]);
                        }
                    }
                }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogSelectedClicked();

                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity().getApplicationContext(), "你选择了取消", Toast.LENGTH_SHORT).show();
                    }
                });
        return builder.create();
    }

}
