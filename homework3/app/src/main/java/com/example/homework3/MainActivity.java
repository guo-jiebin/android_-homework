package com.example.homework3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;

    private String[] genders = new String[]{"男","女"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog;
                dialog = new AlertDialog.Builder(MainActivity.this).setTitle("旧版对话框")
                        .setMessage("这是旧版对话框")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(),"点击了确定",Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(),"点击了取消",Toast.LENGTH_SHORT).show();
                            }
                        })
                        .create();
                dialog.show();
            }
        });

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDialogFrament dialog = new SimpleDialogFrament();
                dialog.show(getSupportFragmentManager(),"simple");
            }
        });



        button3 = (Button)findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChoiceDialogFrament dialog = new ChoiceDialogFrament();
                dialog.setListener(new ChoiceDialogFrament.NotifyDialogListener() {
                    @Override
                    public void onDialogPositiveClicked(String gender) {
                        Toast.makeText(getApplicationContext(),"你选择了"+gender,Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show(getSupportFragmentManager(),"choice");
            }
        });

        button4 = (Button)findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleChoiceDialogFrament dialog = new SingleChoiceDialogFrament();
                dialog.setListener(new SingleChoiceDialogFrament.NotifyDialogListener() {
                    @Override
                    public void onDialogPositiveClick() {
                        Toast.makeText(getApplicationContext(),"你选择了"+dialog.getGender(),Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show(getSupportFragmentManager(),"single");
            }
        });

        button5 = (Button)findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiDialogFragment dialog = new MultiDialogFragment();
                dialog.setListener(new MultiDialogFragment.NotifyDialogListener() {
                    @Override
                    public void onDialogSelectedClicked() {
                        Toast.makeText(getApplicationContext(),"您选择了:"+dialog.getGenders(),Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show(getSupportFragmentManager(),"multi");
            }
        });

        button6 = findViewById(R.id.button6);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialogFragment dialog = new CustomDialogFragment();
                dialog.setListener(new CustomDialogFragment.NotifyDialogListener() {
                    @Override
                    public void onDialogLoginClicked() {
                        Toast.makeText(getApplicationContext(),"您的姓名为"+dialog.getName_edit(),Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.show(getSupportFragmentManager(),"custom");
            }
        });
    }


}